diress
======
Directory Self-Service

This WebUI provides the following user self-service functionality:

- Change Password

Diress is pronounced "duress".


# Configuration
The following configuration options exist:

| Option                | Required  | Description                                                           |
|-----------------------|-----------|-----------------------------------------------------------------------|
| `DOMAIN`              | Yes       | Active Directory DNS Domain (e.g. `contoso.net`)                      |
| `DEFAULT_UPN_SUFFIX`  | No        | UPN suffix to assume for usernames without `@`. Defaults to `DOMAIN`. |
| `PAGE_TITLE`          | No        | Password change page title. Defaults to "Change password on `DOMAIN`. |

These options can be specified in several ways, depending on how Diress is running:

- `config.py`
    - This file is expected in the top-level directory, next to the `diress` package directory.
    - When running under Docker, your `config.py` should be bind-mounted at `/app/config.py`.
- Environment variables


# Deployment

For development, the built-in `flask` webserver can be used:
```
$ FLASK_ENV=development FLASK_APP=diress flask run
```

Running with Docker:
```
$ docker run -e DOMAIN="contoso.net" -p 80:80 diress
```

When running under Docker, you can add CA certificates by bind-mounting (read-only!) to
`/usr/local/share/ca-certificates/`, e.g.
```
$ docker run -e DOMAIN="contoso.net" \
    -v /usr/local/share/ca-certificates:/usr/local/share/ca-certificates:ro \
    -p 80:80 diress
```

# Branding
You can add a custom logo by providing a file named `logo.png` or `logo.jpg`
in the `diress/static/branding/` directory. In Docker:
```
$ docker run -e DOMAIN="contoso.net" \
    -v $(realpath companylogo.jpg):/app/diress/static/branding/logo.jpg:ro \
    -p 80:80 diress
```
