from flask import Flask, render_template, request
from .changepass import change_password, Error, AuthError, ChangePasswordError
from .config import setup_config

app = Flask(__name__)
setup_config(app)

################################################################################
# Routes

@app.route('/', methods=['GET', 'POST'])
def index():
    def tpl(**kw):
        return render_template(
                'main.html',
                **kw)

    if request.method == 'POST':
        # Validate form
        errors = []
        def field(name):
            val = request.form.get(name)
            if not val:
                errors.append("Missing field: {}".format(name))
            return val

        username = field('username')
        old_pass = field('old-password')
        new_pass = field('new-password')
        conf_pass = field('confirm-password')

        if new_pass != conf_pass:
            errors.append("New passwords do not match")

        if errors:
            return tpl(
                alerts = [('error', msg) for msg in errors],
                username = username,
                old_pass = old_pass,
                )

        # Change password
        try:
            change_password(
                domain = app.config['DOMAIN'],
                upn = get_upn(username),
                old_pass = old_pass,
                new_pass = new_pass,
            )
        except AuthError:
            return tpl(
                # clear username, old_pass
                alerts = [('error', 'Username or password is incorrect!')],
                )
        except Error as e:
            return tpl(
                alerts = [('error', str(e))],
                username = username,
                old_pass = old_pass,
                )

        return tpl(
            alerts = [('success', "Password has been changed")],
            )

    return tpl()


################################################################################
# Util

def get_upn(username):
    if '@' in username:
        return username
    return username + '@' + app.config['DEFAULT_UPN_SUFFIX']
