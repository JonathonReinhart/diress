import dns.resolver
from ldap3 import Connection, Server, Tls
from ldap3 import ALL, SIMPLE, SUBTREE
from ldap3.core.exceptions import LDAPBindError, LDAPConstraintViolationResult, \
    LDAPInvalidCredentialsResult, LDAPUserNameIsMandatoryError, \
    LDAPSocketOpenError, LDAPExceptionError, LDAPException
import logging
import ssl

LOG = logging.getLogger(__name__)

class Error(Exception):
    pass

class AuthError(Error):
    """Bad username/password"""
    pass

class ChangePasswordError(Error):
    """Password was not changed"""
    pass

def change_password(domain, upn, old_pass, new_pass):
    try:
        # Connect to LDAP server
        with connect_ldap(domain, user=upn, password=old_pass) as c:
            LOG.debug("Connected ldap: {}".format(c))

            c.bind()
            LOG.debug("Bound ldap: {}".format(c))

            user_dn = find_user_by_upn(c, upn)
            LOG.debug("User DN: {}".format(user_dn))

            c.extend.microsoft.modify_password(user_dn, new_pass, old_pass)

    except LDAPInvalidCredentialsResult as e:
        raise AuthError()

    except LDAPConstraintViolationResult as e:
        # Extract useful part of the error message (for Samba 4 / AD).
        msg = e.message.split('check_password_restrictions: ')[-1].capitalize()
        raise ChangePasswordError(msg)

    except LDAPException as e:
        LOG.exception(e)
        raise Error('Unknown LDAP Error ({})'.format(type(e).__name__))



def find_user_by_upn(conn, upn):
    base = conn.server.info.other['defaultNamingContext'][0]
    search_filter = '(userPrincipalName={})'.format(upn)

    conn.search(base, search_filter, SUBTREE)

    resp = [r for r in conn.response if not r['type'] == 'searchResRef']
    if not resp:
        raise Error("Could not find DN for user {}".format(upn))
    return resp[0]['dn']



def connect_ldap(domain, **kwargs):
    tls = Tls(validate=ssl.CERT_REQUIRED)

    servers = locate_ldap_servers(
            domain,
            use_ssl = True,
            tls = tls,
            connect_timeout = 5,
            get_info = ALL,         # Needed to get defaultNamingContext
            )

    for n, srv in enumerate(servers):
        LOG.debug("Trying LDAP server [{}]: {}".format(n, srv))

        conn = Connection(srv, raise_exceptions=True, **kwargs)

        try:
            conn.open()
        except LDAPSocketOpenError as e:
            LOG.warning("Failed to open connection to server {}: {}".format(srv, e))
        else:
            return conn

    raise Error("No LDAP servers available")



def locate_ldap_servers(domain, **kw):
    srvs = list(locate_service(domain, 'ldap', 'tcp'))
    LOG.debug("Located LDAP servers: {}".format(srvs))

    for host, port in srvs:
        yield Server(
            host = host,
            port = None,    # Ignore port so use_ssl has appropriate effect
            **kw
        )


# From gitlab.com/JonathonReinhart/adman
def locate_service(domain, service, proto='tcp'):
    service = '_' + service
    proto = '_' + proto
    query = '.'.join((service, proto, domain))

    # This can raise dns.resolver.NoAnswer
    records = dns.resolver.query(query, 'SRV')

    # TODO: Sort records by priority (lowest first), then by weight (highest first)

    for r in records:
        yield (r.target.to_text(True), r.port)
