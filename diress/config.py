import os
from pathlib import Path
from flask.logging import default_handler
import logging

from . import version_str

class ConfigError(Exception):
    pass

def setup_config(app):
    # First try to load from config.py located beside the diress/ package, if available
    app.config.from_pyfile('../config.py', silent=True)

    _detect_branding(app)

    # Override with environment vars
    def env_override(env_key, cfg_key=None):
        val = os.getenv(env_key)
        if val is not None:
            app.config[cfg_key or env_key] = val

    env_override('DOMAIN')
    env_override('DEFAULT_UPN_SUFFIX')
    env_override('PAGE_TITLE')

    _check_missing_required(app)
    _apply_defaults(app)
    _setup_root_logging(app)

    app.config['VERSION'] = version_str



def _check_missing_required(app):
    if not 'DOMAIN' in app.config:
        raise ConfigError('DOMAIN not specified')


def _apply_defaults(app):
    if not 'DEFAULT_UPN_SUFFIX' in app.config:
        app.config['DEFAULT_UPN_SUFFIX'] = app.config['DOMAIN']

    if not 'PAGE_TITLE' in app.config:
        app.config['PAGE_TITLE'] = 'Change Password on {}'.format(app.config['DOMAIN'])


def _detect_branding(app):
    """Auto-detect branding elements"""
    static_dir = Path(app.static_folder)
    branding_dir = static_dir / 'branding'
    if not branding_dir.is_dir():
        return

    IMG_EXTS = ("jpg", "png")

    for f in branding_dir.iterdir():
        if not f.is_file():
            continue

        if any(f.name == "logo."+ext for ext in IMG_EXTS):
            app.config['BRANDING_LOGO'] = Path(app.static_url_path) / f.relative_to(static_dir)

def _setup_root_logging(app):
    root = logging.getLogger()
    root.addHandler(default_handler)
    root.setLevel(app.logger.level)
