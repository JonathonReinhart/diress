#!/bin/sh
set -e

# Change to project root
cd $(dirname "$0")/..

IMAGE_NAME="diress"
IMAGE_VER="$(python3 -c 'import diress; print(diress.version_str)')"
IMAGE_TAG="${IMAGE_NAME}:${IMAGE_VER}"

docker build \
    -t "$IMAGE_NAME" \
    -t "$IMAGE_TAG" \
    -f docker/Dockerfile \
    .
