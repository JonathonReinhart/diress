#!/bin/sh

echo "Updating CA certificates..."
update-ca-certificates 2>&1 \
    | grep -v '^WARNING: ca-certificates.crt does not contain exactly one certificate or CRL'

exec waitress-serve-3 --port=80 diress.app:app
